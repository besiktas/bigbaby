from typing import Iterable
import tensorflow.keras as keras

# https://github.com/jw1401/PPO-with-Tensorflow-2.0.0-alpha/blob/master/core/PPO/models.py
def create_conv_base(input_shape: Iterable):
    """
    create keras conv base model we can pass to a2c/ppo/etc
    """
    act_ = 'relu'
    inputs = keras.Input(input_shape)
    x = keras.layers.Conv2D(filters= 32, kernel_size= (5, 5), strides= 1, padding= 'valid', activation=act_, name='conv_name_thing')(inputs)
    x = keras.layers.MaxPooling2D((2, 2))(x)
    x = keras.layers.Conv2D(filters= 64, kernel_size= (3, 3), strides= 2, padding= 'valid', activation= act_)(x)
    x = keras.layers.MaxPooling2D((2, 2))(x)
    outputs = keras.layers.Flatten()(x)
    model = keras.Model(inputs=inputs, ouput=outputs)
    return model
