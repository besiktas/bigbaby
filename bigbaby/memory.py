from typing import Any, Iterable, List, Type
from dataclasses import dataclass, astuple

import numpy as np

@dataclass
class Transition:
    state: Iterable
    action: Any
    reward: Any
    next_state: Iterable

    def get(self):
        return astuple(self)


class Memory:
    """Abstract base class for all implemented memories.

    Do not use this abstract base class directly but instead use one of the concrete memories implemented.

    A memory stores interaction sequences between an agent and one or multiple environments.
    To implement your own memory, you have to implement the following methods:
    """

    def put(self, *args, **kwargs):
        raise NotImplementedError()

    def insert(self, *args, **kwargs):
        raise NotImplementedError()

    def __len__(self):
        raise NotImplementedError()


class Rollout(Memory):
    def __init__(
        self,
        num_steps: int,
        num_processes: int,
        obs_shape: Type,
        # obs_shape: List[int],
        action_space: Type,
        model_loss_shape: int,
    ):

        if action_space.__class__.__name__ == 'Discrete':
            action_shape = 1
        else:
            action_shape = action_space.shape[0]

        self.num_steps = num_steps
        self.num_processes = num_processes
        self.obs_shape = obs_shape
        self.step = 0
        self.action_shape = action_shape
        self.model_loss_shape = model_loss_shape
        self.clear_data()

    def clear_data(self):
        self.obs = np.zeros([self.num_steps + 1, self.num_processes, *self.obs_shape])
        self.values = np.zeros([self.num_steps + 1, self.num_processes, 1])
        self.returns = np.zeros([self.num_steps + 1, self.num_processes, 1])
        self.masks = np.zeros([self.num_steps + 1, self.num_processes, 1])
        self.bad_masks = np.zeros([self.num_steps + 1, self.num_processes, 1])

        self.rewards = np.zeros([self.num_steps, self.num_processes, 1])
        self.logits = np.zeros([self.num_steps, self.num_processes, self.model_loss_shape])
        self.actions = np.zeros([self.num_steps, self.num_processes, self.action_shape])
        self.action_log_probs = np.zeros([self.num_steps, self.num_processes, 1])
        self.step = 0
        self.recurrent_hidden_states = None

    def insert(self, obs, actions, logits, value, rewards, masks, bad_masks) -> None:
        self.obs[self.step + 1] = obs
        self.actions[self.step] = actions
        self.logits[self.step] = logits
        self.values[self.step] = value
        self.rewards[self.step] = rewards
        self.masks[self.step + 1] = masks
        self.bad_masks[self.step + 1] = bad_masks
        self.step += 1


    def after_update(self) -> None:
        old_obs = self.obs[-1]
        old_masks = self.masks[-1]
        old_bad_masks = self.bad_masks[-1]
        old_recurrent_states = None

        self.clear_data()
        self.obs[0] = old_obs
        self.masks[0] = old_masks
        self.old_bad_masks = old_bad_masks



class OnPolicy(Memory):
    """Stores multiple steps of interaction with multiple environments."""

    def __init__(self, steps: int = 1, instances: int = 1):
        self.instances = instances
        self.steps = steps
        self._clear_buffer()
        self.buffers = [[] for _ in range(self.instances)]

    def put(self, transition: Transition, instance=0):
        """Stores transition into the appropriate buffer."""
        self.buffers[instance].append(transition)

    def get(self):
        """Returns all traces and clears the memory."""
        states, actions, rewards, end_states, not_done_mask = [[] for _ in range(5)]
        for proc in self.buffers:
            states.append([trans.state for trans in proc])
            actions.append([trans.action for trans in proc])
            rewards.append([trans.reward for trans in proc])
            end_states.append(proc[-1].next_state)
            not_done_mask.append(
                [1 if trans.next_state is not None else 0 for trans in proc]
            )

        self._clear_buffer()
        return states, actions, rewards, end_states, not_done_mask

    def _clear_buffer(self):
        self.buffers = [[] for _ in range(self.instances)]

    def __len__(self):
        """Returns the number of traces stored."""
        return len(self.buffers[0])
