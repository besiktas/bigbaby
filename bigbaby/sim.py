import numpy as np
from gym.vector import SyncVectorEnv

from bigbaby.logger import logger
from bigbaby.core import Agent
from bigbaby.core import Environment, Memory, Observation, Tensor


class Sim:
    def __init__(
        self,
        env_fn: callable = None,
        env: Environment = None,
        agent: Agent = None,
        processes: int = 1,
    ):
        """A Simulation is the way you use an agent with the environment

        with the classic Markov Decission Process Image you have:

                |-------------------------------------------------------|
                |                     |-------|                         |
                |                     |       |                         |
                |            |------->| Agent |-->|                     |
                |            |        |       |   |                     |
                |            |    |-->|-------|   |                     |
                |            |    |               |                     |
        Sim ==> |   state ==>|    | <== reward    | <== action          |
                |            |    |               |                     |
                |            |    |<--|-------|   |                     |
                |            |        |       |   |                     |
                |            |        |  Env  |<--|                     |
                |            |        |       |                         |
                |            |<-------|-------|                         |
                |-------------------------------------------------------|

        Args:
            env_fn (callable): [description]
            agent (Agent, optional): [description]. Defaults to None.
            processes (int, optional): [description]. Defaults to 1.
        """
        # print("in init")
        self.env_fn = env_fn
        self.env = None
        self.agent = agent
        self.processes = processes
        self._env_created = False

        if env:
            self.env = env
            self._env_created = True

        # dont know if this should be in __init__
        for k in agent._override.keys():
            setattr(self, k, getattr(agent, k))

    def agent_setup(self, num_steps: int, procceses: int):
        """
        create the policy and the memory
        """
        env_ = self.env.envs[0]
        self.agent.setup_for_train(env_, num_steps, procceses)
        self.agent._ready = True

    def env_setup(self, sync: bool = True):
        """
        """
        if not sync:
            logger.info("would use async vector class")
        env = SyncVectorEnv([self.env_fn for i in range(self.processes)])
        self.env = env

    def setup_memory(self, obs: Observation):
        self.agent.memory.obs[0] = np.copy(obs)

    def train(
        self,
        updates: int = 2,
        procceses: int = 1,
        num_steps: int = 5,
        visualize: bool = False,
        plot: callable = None,
    ):
        if not self._env_created:
            self.env_setup()

        # agent = self.agent
        if not self.agent._ready:
            self.agent_setup(num_steps, procceses)

        obs = self.env.reset()
        self.setup_memory(obs)
        for update in range(updates):
            logger.info(f"in train, update={update}")
            self.agent.update_params()

            for step in range(num_steps):
                logger.info(f"in train, step={step}")
                if visualize:
                    logger.info("Gym SyncVectorEnv stuff")

                obs, done = self.agent.act(self.env, obs)

                if done:
                    obs = self.env.reset()

            self.agent.update(obs)


if __name__ == "__main__":
    import gym

    from bigbaby.agent.a2c_tf import A2C

    agent = A2C()
    env_fn = lambda: gym.make("CartPole-v0")
    world = Sim(env_fn=env_fn, agent=agent)
    world.train()

    print("done")
