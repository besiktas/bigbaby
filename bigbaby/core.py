from dataclasses import dataclass
from typing import Generic, Iterable, List, NamedTuple, Type, TypeVar, Mapping

import numpy as np

# Generic Typing thing since I dont understand proper way
Environment = TypeVar("Environment")
Memory = TypeVar("Memory")
Observation = TypeVar("Environment Observation")
Tensor = TypeVar("Tensor")


@dataclass
class Transition:
    """Transition Docstring"""
    state: Tensor
    next_state: Tensor
    action: Tensor
    reward: Tensor


class Agent:
    """
    Abstract Base class for Agent
    """
    _override = {}

    def __init__(self, memory: Memory = None) -> None:
        self._ready = False
        self._policy_ready = False
        self._memory_ready = False
        self.memory = None

    def update_params(self, *args, **kwargs):
        raise NotImplementedError()

    def setup_for_train(self, *args, **kwargs):
        raise NotImplementedError()

    def act(self, *args, **kwargs):
        """act is having agent do action for observation.
            cant decide if agent_experience is a better method name
            since that is what i saw it called in papers
        """
        raise NotImplementedError()

    def train(self, *args, **kwargs):
        raise NotImplementedError()

    def update(self, *args, **kwargs):
        raise NotImplementedError()


class Override:
    """
    not sure if this is ideal but idea is to override functions?
    ideally would be able to pass it bounded method to override but also not
    """
    def __init__(self, fn: callable):
        self.fn = fn

    def __set_name__(self, owner: object, name: str):
        owner._override[name] = self.fn
        # print(f"decorating {self.fn} and using {owner} with {name}")
        # then replace ourself with the original method
        setattr(owner, name, self.fn)