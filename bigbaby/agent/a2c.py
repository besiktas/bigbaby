from typing import Iterable, Type

import numpy as np

from bigbaby.logger import logger
from bigbaby.memory import Rollout
from bigbaby.core import Agent

from bigbaby.core import Environment, Memory, Observation, Tensor


class A2C(Agent):
    """
    """

    def __init__(self, *args, **kwargs):
        """
        A2C Base
        """

        super().__init__()
        self.discount_value = 0.5
        self.gamma = 0.99
        self.entropy = 0.0001
        self.learning_rate = 0.0007

        for key in kwargs.keys():
            setattr(self, key, kwargs[key])

    def setup_for_train(self, env, num_steps: int, processes: int):
        """
        create the policy and then memory
        """

        self.setup_policy(env)
        self.setup_memory(env, num_steps, processes)

    def setup_policy(self, *args, **kwargs):
        raise NotImplementedError

    def setup_memory(self, env: Environment, n_steps: int, processes: int):
        """
        create the memory rollout stuff.
        can do this in backend specific class to make tf/torch specific
        """
        mdl_loss_len = self.extra["mdl_loss_len"]
        obs_space = env.observation_space.shape
        action_space = env.action_space
        self.memory = Rollout(n_steps, processes, obs_space, action_space, mdl_loss_len)

    def act(self, envs, obs: np.array, use_info: bool = False):
        """
        agent experience is the name for agents using observations and acting
        while not training on these experiences yet.  basically collecting data
        for a given policy
        """
        action, value, logits = self.action_value(obs)
        next_obs, reward, done, info = envs.step(action)

        masks = np.array([[0.0] if done_ else [1.0] for done_ in done])
        bad_masks = np.copy(masks)  # FIX
        self.memory.insert(next_obs, action, logits, value, reward, masks, bad_masks)

        return next_obs, done

    def update(self, obs: Observation):
        # action, value, logits
        _, value, _ = self.model.action_value(obs)
        actions_and_advantages = self.compute_actions_advantages(value)
        # actions_and_advantages = self.compute_advantages()
        losses = self.model.train_on_batch(
            np.squeeze(self.memory.obs[:-1]),
            [np.squeeze(actions_and_advantages), np.squeeze(self.memory.returns[:-1])],
        )
        self.memory.after_update()

    def update_params(self) -> None:
        """
        from pytorch-a2c-ppo-acktr stuff:
        update_linear_schedule, etc.
        """
        logger.info("example of updating params params")

    def compute_actions_advantages(self, next_value: Tensor):
        self.memory.returns[-1] = next_value

        for t in reversed(range(self.memory.rewards.shape[0])):
            self.memory.returns[t] = (
                self.memory.rewards[t]
                + self.gamma * self.memory.returns[t + 1] * self.memory.masks[t + 1]
            )

        advantages = self.memory.returns[:-1] - self.memory.values[:-1]
        return np.concatenate([self.memory.actions, advantages], axis=-1)

    def test(self, env, render: bool = False):
        """Test the agent on the environment."""
        # create new environment
        obs = env.reset()
        done = False
        episode_reward = 0.0

        while not done:
            # need to make this consistent and not reshape ideally
            action, value = self.model.action_value(obs.reshape(1, -1))
            obs, reward, done, info = env.step(action)
            episode_reward += reward
            if render:
                env.render()
        return episode_reward
