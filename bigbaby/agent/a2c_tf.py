from typing import Iterable, Type, Tuple

import numpy as np
import tensorflow as tf

from bigbaby.agent.a2c import A2C as A2CBase
from bigbaby.logger import logger

from bigbaby.core import Environment, Memory, Observation, Tensor


class CategoricalProbabilityDistribtution(tf.keras.Model):
    def call(self, logits):
        return tf.squeeze(tf.random.categorical(logits, 1), axis=-1)


class Policy(tf.keras.Model):
    # https://github.com/inoryy/tensorflow2-deep-reinforcement-learning/blob/master/a2c.py
    def __init__(self, obs_shape: Tuple, num_actions: int, conv: bool = False):
        super().__init__("A2C_Policy")

        # basic defaults to make model comparison understandable
        D = {"ACTIVATION": "relu", "HZ": 16}

        if conv:
            logger.info("would use conv_model base")
            # self.base = create_conv_base(input_shape)
        self.hidden1 = tf.keras.layers.Dense(D["HZ"], activation=D["ACTIVATION"])
        self.hidden2 = tf.keras.layers.Dense(D["HZ"], activation=D["ACTIVATION"])
        self.value = tf.keras.layers.Dense(1, activation="linear", name="actor_value")
        self.logits = tf.keras.layers.Dense(
            num_actions, activation="softmax", name="critic_policy"
        )
        self.dist = CategoricalProbabilityDistribtution()

    def call(self, inputs):
        """
        NOTE:
        Actor  - (Policy Network) - logits(hidden_logs)
        Critic - (Value Network)  - value(critic)
        """
        x = tf.convert_to_tensor(inputs)

        actor = self.hidden1(x)
        actor = self.logits(actor)

        critic = self.hidden2(x)
        critic = self.value(critic)

        return actor, critic

    def action_value(self, obs: Observation):
        """
        np.squeeze(action, axis=-1), np.squeeze(value, axis=-1)
        """
        logits, value = self.predict(obs)
        action = self.dist.predict(logits)
        return action, value, logits


class A2C(A2CBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def setup_policy(self, env: Environment):
        """setup the policy that is using tf/keras

        Note:
        can actually name the layers and then just compile with the layer name
        https://medium.com/tensorflow/training-and-serving-ml-models-with-tf-keras-fd975cc0fa27‰
            loss={
                'weather': 'categorical_crossentropy',
                'ground': 'binary_crossentropy'
                })
        ===
        Args:
            env (Environment): [description]

        Returns:
            [type]: [description]
        """

        def _value_loss(returns, value):
            return self.discount_value * tf.keras.losses.mean_squared_error(returns, value)

        def _logits_loss(actions_advantages, logits):
            actions, advantages = tf.split(actions_advantages, 2, axis=-1)
            weighted_sparse_ce = tf.keras.losses.SparseCategoricalCrossentropy(
                from_logits=True
            )
            actions = tf.cast(actions, tf.int32)
            policy_loss = weighted_sparse_ce(actions, logits, sample_weight=advantages)
            entropy_loss = tf.keras.losses.categorical_crossentropy(
                logits, logits, from_logits=True
            )
            return policy_loss - self.entropy * entropy_loss

        optimizer = tf.keras.optimizers.RMSprop(lr=self.learning_rate)

        loss_functions = [_logits_loss, _value_loss]

        policy = Policy(env.observation_space.shape, env.action_space.n)
        policy.compile(optimizer=optimizer, loss=loss_functions)

        # probably abstract this to class later and able to get properties/info
        extra = {"mdl_loss_len": len(loss_functions)}
        self.model = policy
        self.extra = extra

    def action_value(self, obs: Observation):
        return self.model.action_value(obs)



def create_conv_base(input_shape: Iterable):
    """
    create keras conv base model we can pass to a2c/ppo/etc

    from:
    https://github.com/jw1401/PPO-with-Tensorflow-2.0.0-alpha/blob/master/core/PPO/models.py
    """
    act_ = "relu"
    inputs = tf.keras.Input(input_shape)
    x = tf.keras.layers.Conv2D(
        filters=32,
        kernel_size=(5, 5),
        strides=1,
        padding="valid",
        activation=act_,
        name="conv_name_thing",
    )(inputs)
    x = tf.keras.layers.MaxPooling2D((2, 2))(x)
    x = tf.keras.layers.Conv2D(
        filters=64, kernel_size=(3, 3), strides=2, padding="valid", activation=act_
    )(x)
    x = tf.keras.layers.MaxPooling2D((2, 2))(x)
    outputs = tf.keras.layers.Flatten()(x)
    model = tf.keras.Model(inputs=inputs, ouput=outputs)
    return model
