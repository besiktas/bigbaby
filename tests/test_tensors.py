import timeit
import torch
import tensorflow as tf

n = 2**10
m = 2**10
n_times = 1000

printit = False

def test_tf():
    if printit: print(f"Tensorflow Version: {tf.__version__}")
    x = tf.zeros([n, m])

def test_pt():

    if printit: print(f"Torch Version: {torch.__version__}")

    x = torch.zeros([n, m])

# def test_numpy():
#     import numpy as np
#     if printit: print(f"Numpy Version: {np.__version__}")

#     x = np.zeros([n, m])
n = 2
m = 10**3
import random

def test_std():
    a = [[0.0, 0.0] for _ in range(m)]
    # a = [[[0, 0] for _ in range(n_) ]]
    for row in range(len(a)):
        a[row] = [random.random() for _ in range(2)]
    # print(f"length a: {len(a)}, and a: \n{a}")

def test_numpy():
    a = np.zeros([m, n])

    for row in range(len(a)):
        a[row] = np.random.rand(2)

    # print(f"length a: {len(a)}, and a: \n{a}")
    # print(a)

if __name__ == '__main__':
    # test_std()
    print("std time: ", timeit.timeit('test_std()', globals=globals(), number=1000))
    import numpy as np
    print("numpy time: ", timeit.timeit('test_numpy()', globals=globals(), number=1000))
    # print(timeit.timeit("test_tensorflow()", setup="from __main__ import test_tensorflow"))
    # print(timeit.timeit('[func() for func in (test_pytorch,test_tensorflow,test_numpy)]', globals=globals()))
    # print("numpy time: ", timeit.timeit('test_numpy()', globals=globals(), number=1000))
    # print("numpy time: ", timeit.timeit('test_numpy()', globals=globals(), number=1000))
    # print("torch time: ", timeit.timeit('test_pt()', globals=globals(), number=n_times))
    # print("torch time: ", timeit.timeit('test_pt()', globals=globals(), number=n_times))
    # print("tf time: ", timeit.timeit('test_tf()', globals=globals(), number=n_times))
    # print("tf time: ", timeit.timeit('test_tf()', globals=globals(), number=n_times))

