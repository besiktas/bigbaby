from setuptools import find_packages, setup

# https://github.com/facebookresearch/habitat-api/blob/master/setup.py
if __name__ == "__main__":
    setup(
        name="bigbaby",
        packages=find_packages(),
        version="0.0.1",
        install_requires=["numpy", "torch", "tensorflow==2.0.0-beta1"],
    )
