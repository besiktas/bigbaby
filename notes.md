
# Idea
RL Library that works with pytorch/tensorflow2.0 and variety of environments such as gym/habitat-api/etc. where it is easy to extend

In general will stick to numpy but ideally in way that you can extend to tf/torch or convert

# Main Issues/Differences
Differences between tensorflow/pytorch/numpy

### Device
- tensorflow:
  - `with tf.device("cuda"):` to place variable on device
- pytorch
  - `x.to(device)` to place variable on device

### Random

-   tensorflow: tf.random.shuffle shuffles the tensor
-   pytorch torch.randperm is a random permutation of n size

# Other

lots of code from:
https://github.com/ikostrikov/pytorch-a2c-ppo-acktr-gail/
https://github.com/danaugrs/huskarl/

## Libraries

https://github.com/Unity-Technologies/ml-agents/blob/master/ml-agents/mlagents/


## PPO related
https://github.com/Unity-Technologies/ml-agents/blob/master/ml-agents/mlagents/trainers/ppo/trainer.py


## numpy vs std

Often times python array is faster than numpy.array:
```
n = 2
m = 10**3
import random
import numpy as np
def test_std():
    a = [[0.0, 0.0] for _ in range(m)]
    # a = [[[0, 0] for _ in range(n_) ]]
    for row in range(len(a)):
        a[row] = [random.random() for _ in range(2)]

    # print(f"length a: {len(a)}, and a: \n{a}")

def test_numpy():
    a = np.zeros([m, n])

    for row in range(len(a)):
        a[row] = np.random.rand(2)

number_times = 1000
print("std time: ", timeit.timeit('test_std()', globals=globals(), number=number_times))
import numpy as np
print("numpy time: ", timeit.timeit('test_numpy()', globals=globals(), number=number_times))
```

As well as being more versatile


## TODO
https://github.com/Unity-Technologies/ml-agents/blob/master/docs/Readme.md