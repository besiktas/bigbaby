from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv2D, MaxPooling2D, Dropout, Flatten

import huskarl as hk
import habitat

from habitat import SimulatorActions

class SimpleRLEnv(habitat.RLEnv):
    # def __init__()
    # def __call

    def get_reward_range(self):
        return [-1, 1]

    def get_reward(self, observations):
        return 0

    def get_done(self, observations):
        return self.habitat_env.episode_over

    def get_info(self, observations):
        return self.habitat_env.get_metrics()



class NavRLEnv(habitat.RLEnv):
    def __init__(self, config_env, config_baseline=None):
        self._config_env = config_env.TASK
        self.SUCCESS_REWARD = 10.0
        self.SLACK_REWARD = -0.01

        # self._config_baseline = config_baseline
        self._previous_target_distance = None
        self._previous_action = None
        self._episode_distance_covered = None
        super().__init__(config_env)

    def reset(self):
        self._previous_action = None

        observations = super().reset()

        self._previous_target_distance = self.habitat_env.current_episode.info[
            "geodesic_distance"
        ]
        return observations

    def step(self, action):
        self._previous_action = action
        return super().step(action)

    def get_reward_range(self):
        return (
            self.SLACK_REWARD - 1.0,
            self.SUCCESS_REWARD + 1.0,
        )

    def get_reward(self, observations):
        reward = self.SLACK_REWARD

        current_target_distance = self._distance_target()
        # print(f"getting reward, prev: {self._previous_target_distance} and curr: {current_target_distance} ")
        reward += self._previous_target_distance - current_target_distance
        self._previous_target_distance = current_target_distance

        if self._episode_success():
            reward += self._config_baseline.SUCCESS_REWARD
        return reward

    def _distance_target(self):
        current_position = self.habitat_env.sim.get_agent_state().position.tolist()
        target_position = self.habitat_env.current_episode.goals[0].position
        distance = self.habitat_env.sim.geodesic_distance(
            current_position, target_position
        )
        return distance

    def _episode_success(self):
        if (
            self._previous_action == SimulatorActions.STOP
            and self._distance_target() < self._config_env.SUCCESS_DISTANCE
        ):
            return True
        return False

    def get_done(self, observations):
        done = False
        if self.habitat_env.episode_over or self._episode_success():
            done = True
        return done

    def get_info(self, observations):
        return self.habitat_env.get_metrics()



def get_model():
    model = Sequential([
        Conv2D(32, kernel_size=(4, 4), activation='relu', input_shape=input_shape),
        Conv2D(32, (4, 4), activation='relu'),
        MaxPooling2D(pool_size=(2, 2)),
        Dropout(0.25),
        Flatten(),
        Dense(32, activation='relu')
    ])
    return model
# ------------------------------------

from habitat_baselines.common.env_utils import construct_envs

# envs = construct_envs()

# config = habitat.get_config(config_paths="config.yaml")
# create_env = lambda: SimpleRLEnv(config=config).unwrapped
create_env = lambda: NavRLEnv(config_env=config).unwrapped
# create_env = lambda: HuskarlRLEnv(config=config).unwrapped
dummy_env = create_env()

states = dummy_env.reset()
obs = dummy_env.observation_space.sample()
input_shape = obs["rgb"].shape
action_space_n = dummy_env.action_space.n
dummy_env.close()


model = get_model()
agent = hk.agent.DQN(model, actions=action_space_n, nsteps=2)

# These are what would need to work with habitat-api i believe
# sim = hk.Simulation(create_env, agent)
# sim.train(max_steps=30)
# sim.test(max_steps=10)

instances = 1
max_steps = 1000

episode_reward_sequences = []
episode_step_sequences = []
episode_rewards = 0


# def run():

# if __name__=="__main__":
#     envs = create_env()
#     states = envs.reset()


#     for step in range(max_steps):
#         # Most of this is copied from simulation._sp_train()
#         action = agent.act(states["rgb"])
#         next_state, reward, done, other_ = envs.step(action)
#         agent.push(hk.memory.Transition(states["rgb"], action, reward, None if done else next_state["rgb"]))
#         episode_rewards += reward

#         if done:
#             print(f"done, step: {step} episode_rewards: {episode_rewards}")
#             episode_reward_sequences.append(episode_rewards)
#             episode_step_sequences.append(step)
#             episode_rewards = 0
#             states = envs.reset()
#         else:
#             states = next_state
#         if step % 5 == 0: print(f"step: {step}, action: {action}, reward: {reward} and pointgoal is: {states['pointgoal']}")
#         agent.train(step)