import gym

import bigbaby as bb
# from bigbaby.agent.a2c_tf import A2C_TF as A2C
from bigbaby.agent.a2c_tf import A2C

agent = A2C(gamma=0.98)
env_fn = lambda: gym.make("CartPole-v0")
world = bb.Sim(env_fn=env_fn, agent=agent)
world.train()
